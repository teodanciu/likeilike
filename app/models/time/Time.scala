package models.time

import scala.util.{Try, Success}

case class Time(hour: Int, minute: Int) extends Ordered[Time] {

  import TimeFacts._

  require(validHour(hour), s"$hour is not between 0 and 23")
  require(validMinute(minute), s"$minute is not between 0 and 59")

  def compare(that: Time): Int =
    if (this == that) 0
    else if (this.hour > that.hour) 1
    else if (this.hour == that.hour && this.minute > that.minute) 1
    else -1

  override def toString = s"$hour:$minute"
}

object Time {
  import scala.language.implicitConversions

  def apply(hourMinute: String): Time = {
    val tries = hourMinute.split(":").map(s => Try(s.toInt))
    tries match {
      case Array(Success(hour), Success(minute)) => Time(hour, minute)
      case _ => throw new IllegalArgumentException(s"Can't create Time from $hourMinute")
    }
  }
  implicit def stringToTime(hourMinute: String) = Time(hourMinute)
}

object TimeFacts {
  val HoursInDay = 24
  val MinutesInHour = 60

  val validHour: Int => Boolean = hour => hour >= 0 && hour < HoursInDay
  val validMinute: Int => Boolean = minute => minute >= 0 && minute < MinutesInHour

  val normalizedMinute: Int => (Int, Int) = minute => (minute / MinutesInHour, minute % MinutesInHour)
  val normalizedHour: Int => Int = hour => hour % HoursInDay
}
