package models.time

object WeekDay extends Enumeration {
  type WeekDay = Value
  val Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday = Value

  def apply(day: String): WeekDay.Value  = {
    WeekDay withName day
  }
}
