package models.time

import scala.util.{Success, Try}

case class Interval(start: Time, end: Time) {
  require(start < end, s"Start and end of an interval must be within the same day. $start is not  <  $end.")

  def startsAt(time: Time) = start == time
  def startsStrictlyBefore(time: Time) = start < time
  def startsBefore(time: Time) = start <= time
  def startsStrictlyAfter(time: Time) = start > time
  def startsAfter(time: Time) = start >= time
  def endsAt(time: Time) = end == time
  def endsStrictlyBefore(time: Time) = end < time
  def endsBefore(time: Time) = end <= time
  def endsStrictlyAfter(time: Time) = end > time
  def endsAfter(time: Time) = end >= time

  def startsTogetherWith = together(_.start)
  def startsStrictlyBefore = strictlyBefore(_.start)
  def startsBefore = before(_.start)
  def startsStrictlyAfter = strictlyAfter(_.start)
  def startsAfter = after(_.start)

  def endsTogether = together(_.end)
  def endsStrictlyBefore = strictlyBefore(_.end)
  def endsBefore = before(_.end)
  def endsStrictlyAfter = strictlyAfter(_.end)
  def endsAfter = after(_.end)

  def strictlyWithin = and(startsStrictlyAfter, endsStrictlyBefore)
  def within = and(startsAfter, endsBefore)


  type IntervalPredicate = Interval => Boolean
  type IntervalBasedTime = Interval => Time

  private def before(what: IntervalBasedTime): IntervalPredicate = nonstrict(strictlyBefore)(what)
  private def after(what: IntervalBasedTime): IntervalPredicate =  nonstrict(strictlyAfter)(what)

  private def nonstrict(strict: IntervalBasedTime => IntervalPredicate): IntervalBasedTime => IntervalPredicate =
    lift(or)(strict)(together)

  private def together(what: IntervalBasedTime): IntervalPredicate = what(this) == what(_)
  private def strictlyBefore(what: IntervalBasedTime): IntervalPredicate = what(this) < what(_)
  private def strictlyAfter(what: IntervalBasedTime): IntervalPredicate = what(this) > what(_)

  private def lift[A, B](f: (B, B) => B)(g: A => B)(h: A => B): A => B =
   x => f(g(x), h(x))

  private def or(p1: IntervalPredicate, p2: IntervalPredicate): IntervalPredicate =
  i => p1(i) ||  p2(i)

  private def and(p1: IntervalPredicate, p2: IntervalPredicate): IntervalPredicate =
  i => p1(i) && p2(i)

  override def toString = s"[$start, $end]"
}

object Interval {
  def apply(hyphenated: String): Interval = {
    val tries = hyphenated.split("\\s*-\\s*").map(s => Try(Time(s)))
    tries match {
      case Array(Success(start), Success(end)) => Interval(start, end)
      case _ => throw new IllegalArgumentException(s"Can't create Interval from $hyphenated")
    }
  }

  case class HalfInterval(start: Time) {
    def to(time: Time): Interval = Interval(start, time)
    def toEndOf(interval: Interval): Interval = Interval(start, interval.end)
    def toStartOf(interval: Interval): Interval = Interval(start, interval.start)

    def forMinutes(minutes: Int): Interval = Interval(start, computeEnd(minutes))

    private def computeEnd(minutes: Int): Time = {
      import TimeFacts._

      val fullEndMinute = start.minute + minutes

      val (hourOffset, endMinute) = normalizedMinute(fullEndMinute)
      val endHour = normalizedHour(start.hour + hourOffset)

      Time(endHour, endMinute)
    }
  }

  def from(start: Time): HalfInterval = HalfInterval(start)
  def from(interval: Interval): HalfInterval = HalfInterval(interval.start)
}