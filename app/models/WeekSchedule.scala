package models

import models.time.WeekDay
import WeekDay._

class WeekSchedule(val scheduleByDay: Map[WeekDay, DaySchedule]) {

  def apply(weekDay: WeekDay): DaySchedule =
    scheduleByDay.getOrElse(weekDay, DaySchedule.empty)

  override def equals(other: Any): Boolean =
    if (other == null || !other.isInstanceOf[WeekSchedule])
       false
    else
       scheduleByDay == other.asInstanceOf[WeekSchedule].scheduleByDay

  override def toString = scheduleByDay.mkString("\n")
}

object WeekSchedule {
  def apply(activitiesByDay: Map[String, List[(String, String)]]): WeekSchedule = {
    val weekDayMap = for {
      (day, activityList) <- activitiesByDay
    } yield WeekDay(day) -> DaySchedule(activityList)
    new WeekSchedule(weekDayMap.toMap)
  }
}

