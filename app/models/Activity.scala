package models

class Activity(private val label: String) {
  val name = Activity.WordPrefixRegex.findFirstIn(label).getOrElse(label)

  override def equals(other: Any): Boolean =
    if (other == null || !other.isInstanceOf[Activity]) false
    else name == other.asInstanceOf[Activity].name

  override def hashCode = name.hashCode

  override def toString = name
}

object Activity {
  private val WordPrefixRegex = ".*\\w".r

  def apply(label: String): Activity =  {
    new Activity(label)
  }
}