package models

import models.time.{Interval, Time}

case class DaySchedule(activities: Map[Interval, Set[Activity]]) {
  def activitiesStartingAt(time: Time): Set[Activity] =
    collectActivities(_ startsAt time)

  def activitiesStartingBefore(time: Time): Set[Activity] =
    collectActivities(_ startsBefore time)

  def activitiesStartingAfter(time: Time): Set[Activity] =
    collectActivities(_ startsAfter time)

  def activitiesStrictlyWithin(interval: Interval): Set[Activity] =
    activities.getOrElse(interval, Set.empty)

  def activitiesWithin(interval: Interval): Set[Activity] =
    collectActivities(_ within interval)

  private def collectActivities(p: Interval => Boolean): Set[Activity] =
    activities.collect {
      case (i, a) if p(i) => a
    }.flatten.toSet

  //or flatMap that s***. Too beautiful to delete
  /*activities.flatMap{case(i, a) if p(i) => a; case _ => None}.toSet*/

  override def toString = activities.mkString("\n")

  def isEmpty = activities.isEmpty

}


object DaySchedule {
  def apply(activities: List[(String, String)]): DaySchedule = {
    val map =
      activities.groupBy(_._1).map {
        case (intervalString, activityList) =>
          val activities = activityList.map {
            case (_, activityName) => Activity(activityName)
          }
          Interval(intervalString) -> activities.toSet
      }
    DaySchedule(map)
  }

  def empty = DaySchedule(Map.empty[Interval, Set[Activity]])
}