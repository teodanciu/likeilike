package models

case class ClubSchedule(club: Club, schedule: WeekSchedule)

case class Club(name: String)