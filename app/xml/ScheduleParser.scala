package xml

import scala.xml._
import java.io.Reader
import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl
import models.WeekSchedule

object ScheduleParser {

  lazy val xhtmlParser = new SAXFactoryImpl().newSAXParser()
  lazy val xmlLoader = XML.withSAXParser(xhtmlParser)

  /*Map(Monday -> List(("07 - 07.30", "Pilates"), ("09:00 - 10:00", "Kettle Bells")), Tuesday -> List(....))  )*/
  def parseWeekSchedule(reader: Reader): WeekSchedule = {
    val wholeXml = xmlLoader.load(reader)

    val weekContainer = Elements.of(wholeXml).withTag("div").withAttributeClass("ff-daily-gx-timetable-container")
    val dayNodes = weekContainer.withTag("div").withAttributeClass("ff-tab-panel").toNodeSeq
    val map = (for {
      dayNode <- dayNodes
      day: String = Elements.of(dayNode).withTag("p").withAttributeClass("ff-tab-title").textOfFirst
      activityNodes: NodeSeq = Elements.of(dayNode).withTag("table").withAttributeClass("ff-timetable").withTag("tbody").withTag("tr").toNodeSeq
      dayActivities: List[(String, String)] = parseActivities(activityNodes)
    } yield (day, dayActivities)).toMap
    WeekSchedule(map)
  }

  private def parseActivities(activityNodes: NodeSeq): List[(String, String)] = {
    activityNodes.map {
      node =>
        val List(interval, activity) = Elements.of(node).withTag("td").textOfFirst(2)
        (interval, activity)
    }.toList
  }
}