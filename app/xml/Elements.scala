package xml

import scala.xml.{Node, MetaData, NodeSeq}

class Elements(xml: NodeSeq) {

  private def attributeBasedPredicate(att: MetaData => Boolean): Node => Boolean =
    node => node.attributes.exists(att)

  def withTag(name: String): Elements =
    new Elements(xml \\ name)

  def withAttributeEqualTo(name: String)(value: String): Elements = {
    withAttribute(name)(value)(_ == _)
  }

  def withAttribute(name: String)(value: String)(relation: (String, String) => Boolean) = {
    val nodeSeq = xml filter attributeBasedPredicate(att => att.key == name && relation(att.value.text, value))
    new Elements(nodeSeq)
  }

  def withAttributeClass(value: String): Elements =
    withAttribute("class")(value)(_.contains(_))

  def textOfFirst(n: Int): List[String] =
    xml.take(n).map(_.text).toList

  def textOfFirst: String =
    xml.take(1).map(_.text).toList(0)

  def toNodeSeq: NodeSeq = xml

  override def toString = xml.toString
}

object Elements {
  def of(xml: Node): Elements =
    new Elements(xml)
}
