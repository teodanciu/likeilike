package util

import models.{DaySchedule, Activity}
import models.time.Interval

object TestData {

  object Activities {
    val combat = Activity("BODYCOMBAT")
    val core = Activity("Core")
    val pump = Activity("BODYPUMP")
    val kickbox = Activity("Kick Boxing")
    val yoga = Activity("Yoga")
    val weightLoss = Activity("Weight Loss Your Way")
    val victoria = Activity("Victoria Pendleton Team Sprint")
    val spin = Activity("Spin")
    val fitness = Activity("Athletic Fitness")
    val kettle = Activity("Kettle Bells")
    val team = Activity("Team Workout TRX")
    val pilates = Activity("Pilates")
    val zumba = Activity("Zumba")


  }

  object Intervals {
    val halfSeven = Interval("07:30 - 07:45")
    val halfSevenToEight = Interval("07:30 - 08:00")
    val eightToNine = Interval("08:00 - 09:00")
    val halfNine = Interval("09:00 - 09:45")
    val quarterToOne = Interval("12:00 - 12:45")
    val quarterPastNoon = Interval("12:15 - 13:00")
    val atOne = Interval("13:00 - 14:00")
    val halfOne = Interval("13:30 - 13:45")
    val atTwo = Interval("14:05 - 14:50")
    val quarterPastThree = Interval("15:15 - 16:10")
    val halfFive = Interval("17:30 - 18:30")
    val atSix = Interval("18:00 - 18:45")
    val halfSix = Interval("18:30 - 19:30")
    val atEightPm = Interval("20:00 - 21:00")

    val morning = Interval("07:00 - 12:00")
    val noon = Interval("12:00 - 13:00")
    val afternoon = Interval("13:00 - 17:00")
    val evening = Interval("17:00 - 22:00")

    val allDay = Interval("07:00 - 22:00")

    val maximum = Interval("00:00 - 23:59")

  }

  object DaySchedules {

    import Activities._
    import Intervals._

    val monday = new DaySchedule(Map(
      halfSeven -> Set(yoga),
      quarterPastNoon -> Set(combat),
      atOne -> Set(kickbox, core),
      halfOne -> Set(weightLoss, spin)
    ))
  }

  private val activitiesData = List(("Weight Loss Your Way", "12:15 - 13:00"),
    ("BODYCOMBAT", "13:00 - 14:00"),
    ("Core", "13:30 - 13:45"),
    ("BODYPUMP", "17:30 - 18:30"),
    ("Kick Boxing", "18:30 - 19:30"),
    ("Yoga", "20:00 - 21:00"))

}
