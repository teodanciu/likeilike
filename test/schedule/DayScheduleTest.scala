package schedule

import models.time.{Interval, Time}
import models.{Activity, DaySchedule}
import org.scalatest.FunSuite
import Interval._
import util.TestData.Intervals._
import org.scalatest.matchers.ShouldMatchers

class DayScheduleTest extends FunSuite with ShouldMatchers {

  test("Construct DaySchedule from a flat list of pairs (interval, activity).") {
    val list = List(("07:00 - 07:30", "Pilates"), ("07:00 - 07:30", "Kettle Bells"),
      ("08:00 - 09:00", "Yoga"), ("12:00 - 12:45", "Bodycombat"), ("12:00 - 12:45", "Spin"))
    val daySchedule = DaySchedule(list)

    daySchedule should not be 'empty
    daySchedule.activities should contain key Interval("07:00 - 07:30")
    daySchedule.activities should contain key Interval("08:00 - 09:00")
    daySchedule.activities should contain key Interval("12:00 - 12:45")

    daySchedule should equal (new DaySchedule(Map(Interval("07:00 - 07:30") -> Set(Activity("Pilates"), Activity("Kettle Bells")),
      from("08:00").to("09:00") -> Set(Activity("Yoga")),
      from(Time(12, 0)).forMinutes(45) -> Set(Activity("Bodycombat"), Activity("Spin")))))

    daySchedule.activitiesWithin(from("07:00").to("07:30")) should equal (Set(Activity("Pilates"), Activity("Kettle Bells")))
  }

  test("Generate empty schedule from an empty list of pairs.") {
    val daySchedule = DaySchedule(Nil)
    daySchedule should be('empty)
    daySchedule.activitiesWithin(maximum) should be('empty)
  }

  test("Single interval with several activities") {
    val list = List(("08:00 - 08:45", "Pilates"), ("08:00 - 08:45", "Kettle Bells"),
      ("08:00 - 08:45", "Bodycombat"), ("08:00 - 08:45", "Spin"))
    val daySchedule = DaySchedule(list)

    daySchedule should not be 'empty

    daySchedule.activitiesWithin(from("08:00").to("08:45")) should equal (Set(Activity("Pilates"),
      Activity("Kettle Bells"), Activity("Bodycombat"), Activity("Spin")))

    daySchedule should equal (new DaySchedule(Map(from("08:00").to("08:45") ->
      Set(Activity("Pilates"), Activity("Kettle Bells"), Activity("Bodycombat"), Activity("Spin")))))
  }

}
