package schedule

import models.DaySchedule
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import util.TestData.Activities._
import util.TestData.Intervals._
import models.time.Interval._


class ActivityRetrievalTest extends FunSuite with ShouldMatchers {

  val day = new DaySchedule(Map(
    halfSeven -> Set(yoga),
    halfSevenToEight -> Set(weightLoss),
    eightToNine -> Set(kettle, fitness),
    halfNine -> Set(combat, spin),
    quarterPastNoon -> Set(victoria),
    atOne -> Set(kickbox, core),
    halfOne -> Set(weightLoss),
    atTwo -> Set(team),
    quarterPastThree -> Set(core, pump),
    halfFive -> Set(yoga),
    atSix -> Set(combat, spin, kettle),
    halfSix -> Set(pump),
    atEightPm -> Set(yoga)
  ))
  import day._

  test("Retrieve activities exactly within an interval") {
    activitiesStrictlyWithin(noon) should be('empty)
    activitiesStrictlyWithin(halfSix) should equal (Set(pump))
    activitiesStrictlyWithin(atOne) should equal (Set(kickbox, core))
  }

  test("Retrieve activities within an interval") {
    activitiesWithin(morning) should equal (Set(yoga, weightLoss, kettle, fitness, combat, spin))
    day activitiesWithin from(halfSeven).to("12:00") should equal(Set(yoga, weightLoss, kettle, fitness, combat, spin))
  }

  test("Retrieve activities starting at some time") {
    activitiesStartingAt("7:30") should equal (Set(yoga, weightLoss))
    activitiesStartingAt("06:00") should be('empty)
    activitiesStartingBefore(halfNine.start) should equal (Set(yoga, weightLoss, kettle, fitness, combat, spin))
  }

}
