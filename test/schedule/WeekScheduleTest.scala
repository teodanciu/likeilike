package schedule

import models.time.WeekDay
import models.{DaySchedule, WeekSchedule}
import org.scalatest.FunSuite
import util.TestData.Activities._
import util.TestData.Intervals._
import WeekDay._
import org.scalatest.matchers.ShouldMatchers

class WeekScheduleTest extends FunSuite with ShouldMatchers {

  test("Construct WeekSchedule from map") {
    val map: Map[String, List[(String, String)]] = Map("Monday" -> List(("07:30 - 07:45", "Pilates"), ("07:30 - 07:45", "Kettle Bells"),
      ("08:00 - 09:00", "Yoga"), ("12:00 - 12:45", "BODYCOMBAT"), ("12:15 - 13:00", "Spin")),
      "Tuesday" -> List(("09:00 - 09:45", "BODYPUMP"), ("13:30 - 13:45", "Kettle Bells"), ("13:30 - 13:45", "Core")),
      "Wednesday" -> Nil,
      "Thursday" -> List(("07:30 - 07:45", "Spin"), ("08:00 - 09:00", "Pilates"), ("18:00 - 18:45", "BODYCOMBAT"), ("18:00 - 18:45", "Weight Loss Your Way")),
      "Friday" -> List(("13:00 - 14:00", "Core")),
      "Saturday" -> List(("14:05 - 14:50", "Zumba"), ("15:15 - 16:10", "Pilates")),
      "Sunday" -> Nil)
    val parsedWeekSchedule = WeekSchedule(map)
    val monday = new DaySchedule(Map(halfSeven -> Set(pilates, kettle), eightToNine -> Set(yoga), quarterToOne -> Set(combat), quarterPastNoon -> Set(spin)))
    val tuesday = new DaySchedule(Map(halfNine -> Set(pump), halfOne -> Set(kettle, core)))
    val wednesday =  DaySchedule.empty
    val thursday = new DaySchedule(Map(halfSeven -> Set(spin), eightToNine -> Set(pilates), atSix -> Set(combat, weightLoss)))
    val friday = new DaySchedule(Map(atOne -> Set(core)))
    val saturday = new DaySchedule(Map(atTwo -> Set(zumba), quarterPastThree -> Set(pilates)))
    val sunday = DaySchedule.empty


    val builtWeekSchedule = new WeekSchedule(Map(Monday -> monday, Tuesday -> tuesday,
      Wednesday -> wednesday, Thursday -> thursday, Friday -> friday, Saturday -> saturday, Sunday -> sunday))

    for (day <- WeekDay.values) {
      parsedWeekSchedule(day) should equal (builtWeekSchedule(day))
    }

    parsedWeekSchedule should equal (builtWeekSchedule)
  }

}
