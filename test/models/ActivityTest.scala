package models

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class ActivityTest extends FunSuite with ShouldMatchers {

  test("Discard non-word characters at the end of activity names") {
    Activity("Dance £") should equal(Activity("Dance"))
    Activity("BODYCOMBAT™ *") should equal(Activity("BODYCOMBAT"))
  }

  test("Activity names not ending with non-word characters remain unchanged") {
    Activity("Zumba") should equal(Activity("Zumba"))
    Activity("Legs Bums & Tums") should equal(Activity("Legs Bums & Tums"))
    Activity("Freestyle Team Workout Basics") should equal(Activity("Freestyle Team Workout Basics"))
  }

  test("In the absurd case of only non-word characters, name remains unchanged") {
    Activity("#@$!*") should equal(Activity("#@$!*"))
  }

}
