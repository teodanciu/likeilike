package time

import models.time.{Interval, Time}
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class TimeHandlingTest extends FunSuite with ShouldMatchers {

  test("Construct Time objects from strings, throwing IllegalArgumentException if not parseable.") {
    Time("14:30") should equal (Time(14, 30))

    Time("15:0") should equal (Time(15, 0))
    Time("15:00") should equal (Time(15, 0))

    shouldBeIllegalArgument(Time(13, 61))
    shouldBeIllegalArgument(Time(13, 61))

    shouldBeIllegalArgument(Time(" "))
    shouldBeIllegalArgument(Time("21-20"))
    shouldBeIllegalArgument(Time("21"))
  }


  test("Compare Time objects") {
    Time("18:00") should equal (Time("18:00"))

    Time("20:30") should be > Time("20:18")
    Time("20:30") should be > Time("00:18")
    Time("20:30") should be >= Time("00:18")
    Time("20:30") should be >= Time("20:30")

    Time("20:30") should be < Time("20:31")
    Time("20:30") should be <= Time("20:31")
    Time("20:30") should be <= Time("20:30")

    Time("18:01") should be <= Time("18:02")
    Time("18:02") should be >= Time("18:01")
  }


  test("Construct Interval objects from Strings.") {
    Interval("12:15 - 13:00") should equal (Interval(Time(12, 15), Time(13, 0)))
    Interval("12:15 - 13:00") should equal (Interval(Time("12:15"), Time("13:00")))
    Interval("12:15 - 13:00") should equal (Interval("12:15", "13:00"))

    Interval("14:00-15:30") should equal (Interval(Time(14, 0), Time(15, 30)))

    shouldBeIllegalArgument(Interval("14:00--15:30"))
    shouldBeIllegalArgument(Interval("14:0d-15:30"))
    shouldBeIllegalArgument(Interval("14:00-13:30"))
  }


  test("Construct Interval objects.") {
    import Interval._
    val i1 = from(Time(20, 30)).to(Time(21, 30))
    i1.start should equal (Time(20, 30))
    i1.end should equal (Time(21, 30))

    from("20:30").to("21:30") should equal (Interval("20:30", "21:30"))

    val i2 = from("18:00").to("18:45")
    i2.start should equal (Time(18, 0))
    i2.end should equal (Time(18, 45))
    i2 should equal (Interval("18:00 - 18:45"))

    from(Time(20, 30)).to(Time(20, 50)) should equal (Interval(Time(20, 30), Time(20, 50)))
  }

  test("Construct Interval objects based on other intervals.") {
    import Interval._
    import util.TestData.Intervals._

    from(halfSeven).start should equal (from("07:30").start)
    from(halfSeven).start should equal (Time("07:30"))

    from("07:30").toStartOf(eightToNine) should equal  (Interval("07:30 - 08:00"))
    from("07:30").toEndOf(eightToNine) should equal (Interval("07:30 - 09:00"))

    from(morning).toEndOf(evening) should equal (allDay)
    from(noon).toEndOf(noon) should equal (noon)

    shouldBeIllegalArgument(from(noon).toStartOf(noon))
  }


  test("Throw IllegalArgumentException in case of invalid Intervals.") {
    import Interval._
    shouldBeIllegalArgument(from("20:00").to("18:00"))
    shouldBeIllegalArgument(from("18:00").to("18:00"))
    shouldBeIllegalArgument(from("25:00").to("27:00"))
    shouldBeIllegalArgument(from("21:00").to("25:00"))

    shouldBeIllegalArgument(from("18:00").to("17"))
    shouldBeIllegalArgument(from("18:00").to(" "))
    shouldBeIllegalArgument(from(" ").to("20:00"))
    shouldBeIllegalArgument(from(" ").to("abc"))
  }


  test("Construct duration-based Interval objects.") {
    import Interval._
    from("20:30").forMinutes(20) should equal (from("20:30").to("20:50"))

    from("20:40").forMinutes(30).end should equal (Time("21:10"))
    from("20:50").forMinutes(10).end should equal (Time("21:00"))
    from("00:15").forMinutes(1).end should equal (Time("00:16"))
    from("13:00").forMinutes(60).end should equal  (Time("14:00"))

    from("13:00").forMinutes(65).end should equal (Time("14:05"))
    from("14:00").forMinutes(70).end should equal (Time("15:10"))
    from("14:00").forMinutes(120).end should equal (Time("16:00"))

    from("23:50").forMinutes(9).end should equal (Time("23:59"))
    shouldBeIllegalArgument(from("23:00").forMinutes(60))
    shouldBeIllegalArgument(from("23:00").forMinutes(120).end)

    shouldBeIllegalArgument(from("15:00").forMinutes(0))
  }


  test("Compare Interval objects") {
    import Interval._
    import util.TestData.Intervals._

    assert(halfSeven startsTogetherWith halfSevenToEight)
    assert(halfSeven startsBefore halfSevenToEight)
    assert(halfSeven startsBefore eightToNine)
    assert(halfSeven startsStrictlyBefore eightToNine)

    assert(halfSeven startsAfter halfSevenToEight)
    assert(halfNine startsAfter halfSeven)
    assert(halfNine startsStrictlyAfter halfSeven)

    assert(from("13:00").to("14:00") startsStrictlyBefore from("13:01").to("14:00"))

    assert(halfSeven endsBefore noon)
    assert(halfSeven endsStrictlyBefore noon)
    assert(noon endsBefore noon)

    assert(halfSevenToEight within morning)
    assert(halfSevenToEight strictlyWithin morning)

    assert(quarterPastNoon within noon)
    assert(halfSix within halfSix)

    assert(morning within allDay)
    assert(noon within allDay)
    assert(evening within allDay)

  }


  def shouldBeIllegalArgument(f: => Unit) {
    intercept[IllegalArgumentException](f)
  }

}
