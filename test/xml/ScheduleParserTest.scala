package xml

import org.scalatest.FunSuite
import ScheduleParser._
import org.scalatest.matchers.ShouldMatchers
import models.time.WeekDay
import scala.io.Source
import java.io.File

class ScheduleParserTest extends FunSuite with ShouldMatchers {

  val reader = Source.fromFile(new File("test/resources/timetable.xml")).reader()
  val weekSchedule = parseWeekSchedule(reader)
  val map = weekSchedule.scheduleByDay

  test("Parse current Bloomsbury-club schedule") {
    map should not be 'empty
  }

  test("All days have an associated non-empty schedule") {
    for (weekday <- WeekDay.values) {
      map should contain key weekday
      map(weekday) should not be 'empty
    }
  }
}
