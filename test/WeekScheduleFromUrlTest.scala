import java.io.File
import models.time.WeekDay
import models.{DaySchedule, WeekSchedule}
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import scala.io.Source
import xml.ScheduleParser
import WeekDay._
import util.TestData.Intervals._
import util.TestData.Activities._

class WeekScheduleFromUrlTest extends FunSuite with ShouldMatchers {

  private val testResourcesPath = new File("test/resources").getAbsolutePath

  test("Request Bloomsbury schedule page and parse it as WeekSchedule") {
    val weekSchedule = requestAndParseLocalFile("bloomsbury.html")
    val monday = weekSchedule(Monday)
    monday activitiesStartingAt "18:00"  should contain (combat)
    monday activitiesStartingBefore "18:15" should contain (combat)

    monday activitiesStartingAfter "18:00" should contain (combat)
    monday activitiesStartingAfter "18:00" should contain (pump)
  }

  test("Camden") {
    val weekSchedule = requestAndParseLocalFile("camden.html")
    weekSchedule(Saturday) activitiesWithin morning should contain (pilates)
  }

  test("Oxford Circus") {
    val weekSchedule = requestAndParseLocalFile("oxford-circus.html")
    weekSchedule(Thursday) activitiesStartingAfter "18:00" should contain (combat)
  }

  private def requestAndParse(url: String): WeekSchedule = {
    val reader = Source.fromURL(url).reader()
    ScheduleParser.parseWeekSchedule(reader)
  }

  private def requestAndParseLocalFile(name: String): WeekSchedule = {
    requestAndParse(s"file://$testResourcesPath/$name")
  }


}
