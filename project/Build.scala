import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "likeilike"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    "net.databinder.dispatch" %% "dispatch-tagsoup" % "0.11.0",
    "org.scalatest" %% "scalatest" % "1.9.1" % "test"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    scalacOptions += "-feature").settings(ScctPlugin.instrumentSettings : _*)
}
